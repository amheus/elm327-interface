import datetime
import os
import json
from influxdb import InfluxDBClient
import configparser

ROOT_DIRECTORY = f"{os.path.dirname(os.path.dirname(__file__))}"

CONFIGURATION = configparser.ConfigParser()
CONFIGURATION.read(f"../config.ini")


def get_dump_file_contents() -> list[dict]:
    global CONFIGURATION

    directory = f"{ROOT_DIRECTORY}/dumps".replace('\\', '/')
    file = open(f"{directory}/{CONFIGURATION['Target']['VIN']}.{CONFIGURATION['Target']['Timestamp']}.dump", "r")
    temp = file.readlines()
    file.close()
    return [json.loads(x.replace("\n", "")) for x in temp]


def build_influxdb_payload(dump: list[dict]) -> list:
    global CONFIGURATION

    payload_builder = []

    for x in dump:
        payload_builder.append({
            "measurement": CONFIGURATION["InfluxDB"]["Measurement"],
            "tags": {
                "VIN": CONFIGURATION["Target"]["VIN"],
            },
            "time": datetime.datetime.fromtimestamp(x["Time"]),
            "fields":      {
                list(x.keys())[1]: x[list(x.keys())[1]]
            }
        })

    return payload_builder


def write_payload_to_database(points: list) -> None:
    global CONFIGURATION

    print(CONFIGURATION)

    influxdb_client = InfluxDBClient(
        host=CONFIGURATION["InfluxDB"]["Host"],
        port=int(CONFIGURATION["InfluxDB"]["Port"]),
        username=CONFIGURATION["InfluxDB"]["Username"],
        password=CONFIGURATION["InfluxDB"]["Password"],
    )

    influxdb_client.create_database(CONFIGURATION["InfluxDB"]["Database"])
    influxdb_client.switch_database(CONFIGURATION["InfluxDB"]["Database"])

    influxdb_client.write_points(
        points=points,
        database=CONFIGURATION["InfluxDB"]["Database"]
    )

    influxdb_client.close()


def main() -> None:
    dump = get_dump_file_contents()
    influxdb_payload = build_influxdb_payload(
        dump=dump
    )
    write_payload_to_database(
        points=influxdb_payload
    )
    print(influxdb_payload)


if __name__ == '__main__':
    main()
