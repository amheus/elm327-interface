
# Arduino
## Required Packages
### From Library Manager
- `SD` (1.2.4)
- `SparkFun Qwiic RTC RV8803 Arduino Library` (1.2.8)
- `SparkFun SerLCD Arduino Library` (1.0.09)
- `SparkFun u-blox GNS3 Arduino Library` (2.2.24)
### Manual Installation

## Config
`C:/Users/{username}/Documents/Arduino/libraries/SparkFun_CAN-Bus_Arduino_Library-master/src/defaults.h`
```h
#ifndef	DEFAULTS_H
#define	DEFAULTS_H

#define	P_MOSI	B,2
#define	P_MISO	B,3
#define	P_SCK	B,1

//#define	MCP2515_CS			D,3	// Rev A
#define	MCP2515_CS			B,0 // Rev B
#define	MCP2515_INT			D,2
#define LED2_HIGH			B,0
#define LED2_LOW			B,0

#endif	// DEFAULTS_H

```
