
#include "pid_wrapper.h"
#include "arithmetic_helpers.h"

#include <SparkFun_RV8803.h>
#include <SparkFun_u-blox_GNSS_Arduino_Library.h>
#include <Adafruit_ISM330DHCX.h>
#include <Canbus.h>
#include <mcp2515.h>
#include <mcp2515_defs.h>
#include <SerLCD.h>

#include <Wire.h>
#include <SD.h>
#include <SPI.h>

/*
Sd2Card card;
SdVolume volume;
SdFile root;
*/

SerLCD lcd;
RV8803 rtc;
SFE_UBLOX_GNSS gps;
Adafruit_ISM330DHCX ism330dhcx;

#define	P_MOSI	B,2
#define	P_MISO	B,3
#define	P_SCK	B,1

#define PIN_BUZZER 30
#define PIN_SD_CARD_CHIP_SELECT 9
#define PIN_LIGHT_BUILTIN 6
#define PIN_LIGHT_CANBUS_LEFT 8
#define PIN_LIGHT_CANBUS_RIGHT 7
#define PIN_LIGHT_DATA_LOG_ALERT 6

#define CONFIG_INIT_STEP_DELAY 750

int currentBacklightRed,currentBacklightGreen,currentBacklightBlue = -1;
RecordedSensorDataCradle CurrentSensorReadings;
String LogFileName;

uint32_t sdCardSize;
uint32_t spaceRemainingOnSdCardAtStart;
uint32_t totalDataWrittenToSDCard = 0;

void updateLCDBacklight(int r, int g, int b)
{
  if (r == currentBacklightRed && g == currentBacklightGreen && b == currentBacklightBlue) return;
  currentBacklightRed = r;
  currentBacklightGreen = g;
  currentBacklightBlue = b;
  lcd.setBacklight(r, g, b);
  delay(250);
}


String padRight(String targetString)
{
  for(int i = 0; i < (16 - targetString.length()); i++)
  {
    targetString += ' ';  
  }
  return targetString;
}

void displayTextOnLCD(String line1, String line2)
{
  lcd.setCursor(0, 0);
  lcd.print(padRight(line1));
  lcd.setCursor(0, 1);
  lcd.print(padRight(line2));
}


void displayFatalErrorMessage(String message)
{
  updateLCDBacklight(255, 0, 0);
  displayTextOnLCD("  FATAL ERROR!  ", message);

  while (true)
  {
    digitalWrite(PIN_LIGHT_CANBUS_LEFT, HIGH);
    digitalWrite(PIN_LIGHT_CANBUS_RIGHT, LOW);
    delay(500);
    digitalWrite(PIN_LIGHT_CANBUS_LEFT, LOW);
    digitalWrite(PIN_LIGHT_CANBUS_RIGHT, HIGH);
    delay(500);
  }
}

void displayErrorMessage(String message)
{
  updateLCDBacklight(255, 140, 0);
  displayTextOnLCD("     ERROR!     ", message);
  delay(2500);
  displayTextOnLCD("                ", "                ");
}


void processValues(byte targetHexValue, byte results[]) {
  PIDInfo targetPID = getPIDInfoFromHexValue(targetHexValue);

  int decodedResults[sizeof(results)];
  for (int i = 0; i < sizeof(results); i++) {
    decodedResults[sizeof(results) - i] = int(results[i]);
  }

  for (int i = 0; i < targetPID.NumberOfParameters; i++) {
    double result = evaluateFormula(targetPID.PIDParameters[i].Formula, decodedResults);
    Serial.print(result);
    Serial.println(targetPID.PIDParameters[i].UnitOfMeasurement);
  }
}

void setup()
{
  Serial.begin(9600);
  Serial.println("Initialising...");
  Wire.begin();
  lcd.begin(Wire);
  delay(CONFIG_INIT_STEP_DELAY);
  updateLCDBacklight(255, 255, 255);
  lcd.setContrast(5);
  lcd.clear();

  // pin config
  delay(CONFIG_INIT_STEP_DELAY);
  updateLCDBacklight(255, 255, 255);
  displayTextOnLCD("Init...    (1/7)", "Pinout");
  
  pinMode(PIN_SD_CARD_CHIP_SELECT, OUTPUT);
  pinMode(PIN_BUZZER, OUTPUT);
  pinMode(PIN_LIGHT_CANBUS_RIGHT, OUTPUT);
  pinMode(PIN_LIGHT_CANBUS_RIGHT, OUTPUT);
  pinMode(PIN_LIGHT_DATA_LOG_ALERT, OUTPUT);


  // CANBUS INIT
  delay(CONFIG_INIT_STEP_DELAY);
  updateLCDBacklight(255, 255, 255);
  displayTextOnLCD("Init...    (2/7)", "Canbus");
  if(!Canbus.init(CANSPEED_500)) displayErrorMessage("CANBUS INIT FAIL");


  // SDCARD INIT
  delay(CONFIG_INIT_STEP_DELAY);
  updateLCDBacklight(255, 255, 255);
  displayTextOnLCD("Init...    (3/7)", "SD Card");
  digitalWrite(PIN_SD_CARD_CHIP_SELECT, HIGH);
  if (!SD.begin(PIN_SD_CARD_CHIP_SELECT)) displayFatalErrorMessage("    SD INIT FAIL");
  // if (!card.init(SPI_HALF_SPEED, PIN_SD_CARD_CHIP_SELECT)) displayFatalErrorMessage("SDCARD INIT FAIL");
  // if (!volume.init(card)) displayFatalErrorMessage("SD VOL INIT FAIL");
  // root.openRoot(volume);
  // sdCardSize = ((volume.blocksPerCluster() * volume.clusterCount()) / 2) * 1024;


  // GPS INIT
  delay(CONFIG_INIT_STEP_DELAY);
  updateLCDBacklight(255, 255, 255);
  displayTextOnLCD("Init...    (4/7)", "GPS");
  if (gps.begin() == false) displayErrorMessage("   GPS INIT FAIL");
  gps.setI2COutput(COM_TYPE_UBX);
  gps.saveConfigSelective(VAL_CFG_SUBSEC_IOPORT);


  // ACCELEROMETER INIT
  delay(CONFIG_INIT_STEP_DELAY);
  updateLCDBacklight(255, 255, 255);
  displayTextOnLCD("Init...    (5/7)", "Accelerometer");
  if (!ism330dhcx.begin_I2C())  displayErrorMessage("  GRYO INIT FAIL");
  ism330dhcx.configInt1(false, false, true);
  ism330dhcx.configInt2(false, true, false);


  // RTC INIT
  delay(CONFIG_INIT_STEP_DELAY);
  updateLCDBacklight(255, 255, 255);
  displayTextOnLCD("Init...    (6/7)", "RTC");
  if (!rtc.begin()) displayErrorMessage("   RTC INIT FAIL");


  // SET RTC TIME
  delay(CONFIG_INIT_STEP_DELAY);
  updateLCDBacklight(255, 255, 255);
  displayTextOnLCD("Init...    (7/7)", "RTC Time Setting");
  if (!rtc.setToCompilerTime()) displayErrorMessage("CANNOT SET TIME");

  delay(CONFIG_INIT_STEP_DELAY);
  updateLCDBacklight(255, 255, 255);
  lcd.clear();

  rtc.updateTime();
  LogFileName = "NO_VIN_" + String(rtc.getEpoch()) + ".txt";

  Serial.println("Init completed");
}


void getGPSReadings()
{
  CurrentSensorReadings.longitude = gps.getLatitude();
  CurrentSensorReadings.latitude = gps.getLongitude();
  CurrentSensorReadings.altitude = gps.getAltitude();
  CurrentSensorReadings.siv = gps.getSIV();
}
void getAccellerometerReadings()
{
  sensors_event_t accel;
  sensors_event_t gyro;
  sensors_event_t temp;
  ism330dhcx.getEvent(&accel, &gyro, &temp);
  // CurrentSensorReadings.temperature = temp;
  CurrentSensorReadings.acceleration_x = accel.acceleration.x;
  CurrentSensorReadings.acceleration_y = accel.acceleration.y;
  CurrentSensorReadings.acceleration_z = accel.acceleration.z;
  CurrentSensorReadings.gyro_x = accel.gyro.x;
  CurrentSensorReadings.gyro_y = accel.gyro.y;
  CurrentSensorReadings.gyro_z = accel.gyro.z;
}
void getVehicleData()
{

}



void logData(String field, double value)
{
  rtc.updateTime();
  digitalWrite(PIN_LIGHT_DATA_LOG_ALERT, LOW);
  File dataFile = SD.open(LogFileName, FILE_WRITE);
  dataFile.print(rtc.getEpoch());
  dataFile.print("::");
  dataFile.print(field);
  dataFile.print("::");
  dataFile.println(value);
  dataFile.close();

  totalDataWrittenToSDCard += sizeof(rtc.getEpoch()) + 2 + field.length() + 2 + sizeof(value);

  digitalWrite(PIN_LIGHT_DATA_LOG_ALERT, HIGH);
}


int currentScreen = 0;
void renderDisplay()
{
  lcd.clear();
  switch(currentScreen)
  {
    case 0:
      // displayTextOnLCD("                ", "                ");
      displayTextOnLCD("Data Recorded:  ", String(totalDataWrittenToSDCard) + " bytes");
      break;
    case 1:
      // displayTextOnLCD("Space left on SD", String((totalDataWrittenToSDCard - sdCardSize)/1024) + " Kb");
      break;
    case 2:
      displayTextOnLCD(String(CurrentSensorReadings.longitude), String(CurrentSensorReadings.latitude));
      break;
  }
  currentScreen++;
  if (currentScreen > 1) currentScreen = 0;
}


long lastTime = 0;
void loop()
{
  getAccellerometerReadings();
  getVehicleData();
  if (millis() - lastTime > 5000)
  {
    lastTime = millis();
    getGPSReadings();

    logData("GPS_LON", CurrentSensorReadings.longitude);
    logData("GPS_LAT", CurrentSensorReadings.latitude);
    logData("ACCEL_X", CurrentSensorReadings.acceleration_x);
    logData("ACCEL_Y", CurrentSensorReadings.acceleration_y);
    logData("ACCEL_Z", CurrentSensorReadings.acceleration_z);
    logData("GYRO_X", CurrentSensorReadings.gyro_x);
    logData("GYRO_Y", CurrentSensorReadings.gyro_y);
    logData("GYRO_Z", CurrentSensorReadings.gyro_z);

    renderDisplay();
  }
}