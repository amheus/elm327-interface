
struct PIDParameters {
  double MinimumValue;
  double MaximumValue;
  const char* Formula;
  const char* UnitOfMeasurement;
};

struct PIDInfo {
  byte HexValue;
  int NumberOfBytes;
  int NumberOfParameters;
  PIDParameters PIDParameters[4];
};

struct BitEncodingInfo {
  byte HexValue;
  char ShortDescription[16];
};

struct BitEncodedPIDInfo {
  byte HexValue;
  BitEncodingInfo Encodings[0xFF];
};



struct RecordedSensorDataCradle {
    long longitude;
    long latitude;
    long altitude;
    int siv;

    int temperature;
    int acceleration_x;
    int acceleration_y;
    int acceleration_z;
    int gyro_x;
    int gyro_y;
    int gyro_z;
};

