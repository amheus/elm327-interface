from elm327_interface.parameter_ids.current_data_commands import CurrentDataCommand


def evaluate_formula(algebraic_formula: str, values: list) -> float:
    variables = {}
    for i, value in enumerate(values):
        variables[chr(65 + i)] = value

    for var, val in variables.items():
        algebraic_formula = algebraic_formula.replace(var, str(val))

    try:
        result = eval(algebraic_formula)
        return result
    except Exception as e:
        raise f"Error: {e}"


def parse_raw_elm327_result(command: CurrentDataCommand, raw_result: bytearray):
    command_sent_ish, result = [b''.join(raw_result[:2]), b''.join(raw_result[2:])]

    if command.value.FORMULA == "0 + 1 * A" and command.value.DECODER is not None:
        return command.value.DECODER(result), True

    return evaluate_formula(command.value.FORMULA, [int(result, 16)]), False
