import json
from datetime import datetime
from time import sleep
import os

from elm327_interface.parameter_ids.current_data_commands import CurrentDataCommand

from elm327_interface.elm327_interface import ELM327Interface
from elm327_interface.parameter_ids.service_modes import ServiceMode
from helpers.formula_helpers import parse_raw_elm327_result


def should_log_reading(target: CurrentDataCommand, result: float) -> bool:
    global LAST_READINGS
    should_log = False

    if target.value.MIN_VALUE is not None and result < target.value.MIN_VALUE:
        should_log = True
    if target.value.MAX_VALUE is not None and result > target.value.MAX_VALUE:
        should_log = True

    if target.value.HEX not in LAST_READINGS:
        LAST_READINGS[target.value.HEX] = result
        return True

    if result < LAST_READINGS[target.value.HEX] * (1 - target.value.VARIATION[0]):
        should_log = True
    if result > LAST_READINGS[target.value.HEX] * (1 + target.value.VARIATION[1]):
        should_log = True

    if LAST_READINGS.get(target.value.HEX) is None or should_log:
        LAST_READINGS[target.value.HEX] = result

    return should_log


def log_reading(target: CurrentDataCommand, result: float):
    global ELM327_CONNECTION

    if target.value.ROUND_TO_NUMBER_OF_DECIMALS is not None:
        result = round(number=result, ndigits=target.value.ROUND_TO_NUMBER_OF_DECIMALS)

    datetime_to_log = str(datetime.utcnow().timestamp()).split('.')[0]
    data_to_write = f"{datetime_to_log},{target.value.HEX},{result};"

    DUMP_FILE_CONNECTION.write(f"{data_to_write}\n")

    print(f"{target.name} -> {result}")


def go(target: CurrentDataCommand):
    global LAST_READINGS

    result = ELM327_CONNECTION.go_get(ServiceMode.SHOW_CURRENT_DATA, target)
    if result is False:
        return None

    decoded_result, is_an_encoded_response = parse_raw_elm327_result(target, result)

    if is_an_encoded_response:
        if LAST_READINGS.get(target.value.HEX, None) != decoded_result:
            LAST_READINGS[target.value.HEX] = decoded_result
            log_reading(target=target, result=decoded_result)
            return decoded_result
        return None

    if should_log_reading(target=target, result=decoded_result):
        log_reading(target=target, result=decoded_result)
        return decoded_result
    return None


def main() -> None:
    while True:
        # go(CurrentDataCommand.FUEL_SYSTEM_STATUS)
        for command in ELM327_CONNECTION.SUPPORTED_PIDS:
            go(command)
        # exit(0)
        sleep(1)


if __name__ == '__main__':
    ELM327_CONNECTION = ELM327Interface()
    ELM327_CONNECTION.initialise_interface()

    LAST_READINGS = {}
    DUMP_FILE_DIRECTORY = f"{os.path.dirname(os.path.dirname(__file__))}/dumps".replace('\\', '/')
    if not os.path.exists(DUMP_FILE_DIRECTORY):
        os.mkdir(DUMP_FILE_DIRECTORY)
    DUMP_FILE_CONNECTION = open(f"{DUMP_FILE_DIRECTORY}/{ELM327_CONNECTION.VEHICLE_IDENTIFICATION_NUMBER}.{datetime.utcnow().timestamp()}.dump", "w")

    main()

    ELM327_CONNECTION.terminate()
    DUMP_FILE_CONNECTION.close()
