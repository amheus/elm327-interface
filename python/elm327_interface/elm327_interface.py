import time

import serial

from elm327_interface.parameter_ids.current_data_commands import CurrentDataCommand
from elm327_interface.parameter_ids.service_modes import ServiceMode


class ELM327Interface:
    TARGET: str
    BAUD_RATE: int

    NEWLINE_DELIMITER = "\r"
    LOGGING = False
    ENCODING = "ASCII"

    SERIAL_CONNECTION: serial.Serial

    VEHICLE_IDENTIFICATION_NUMBER: str
    SUPPORTED_PIDS: list[CurrentDataCommand]

    CONTROL_CHAR = ">"

    def __init__(self, target: str = "/dev/ttyUSB0", baud_rate: int = 38400):
        self.TARGET = target
        self.BAUD_RATE = baud_rate

        self.SERIAL_CONNECTION = serial.Serial(self.TARGET, self.BAUD_RATE)
        self.SERIAL_CONNECTION.timeout = 2.5

    def get_vin(self):
        response = self.raw_send_recieve(message="0902")
        result = response.decode(self.ENCODING).split('\r')[2:5]

        vin_hex = "".join(x[3:].replace(" ", "") for x in result)

        vin_ascii = bytes.fromhex(vin_hex).decode('ASCII')[1:]

        control_chars = ''.join([chr(i) for i in range(32)])
        vin_ascii = vin_ascii.lstrip('\x00' + control_chars)

        self.VEHICLE_IDENTIFICATION_NUMBER = vin_ascii

    def get_supported_pids(self) -> list[CurrentDataCommand]:
        def bytes_to_binary_string(hex_byte_list):
            binary_string = ""
            for hex_byte in hex_byte_list:
                # Convert the hexadecimal string to bytes
                byte_value = bytes.fromhex(hex_byte.decode('utf-8'))
                # Use the format function to convert the byte to an 8-bit binary string
                binary_byte = format(int.from_bytes(byte_value, byteorder='big'), '08b')
                binary_string += binary_byte
            return binary_string

        def process_supported_pids_response(response: list[bytes], range_start, range_end):
            if response is False:
                return []
            temp = (bytes_to_binary_string(response[2:]))
            builder = []
            for x in range(range_start, range_end):
                if temp[x-range_start] == "1":
                    builder.append(x)
            return builder

        def find_command_enum_from_hex(target_value) -> CurrentDataCommand:
            for member in CurrentDataCommand.__members__.values():
                if member.value.HEX == hex(target_value)[2:].upper():
                    return member
            return None

        supported_builder = []
        supported_but_missing_builder = []
        uwu = [
            ["0100", 0x01, 0x20],
            ["0120", 0x21, 0x40],
            ["0140", 0x41, 0x60],
            ["0160", 0x61, 0x80],
            ["0180", 0x81, 0xA0],
            ["01A0", 0xA1, 0xC0],
            ["01C0", 0xC1, 0xE0],
        ]
        for x in uwu:
            supported_pids = process_supported_pids_response(self.go_get_raw(x[0]), x[1], x[2])
            for xx in supported_pids:
                target_enum = find_command_enum_from_hex(xx)
                if target_enum is not None:
                    supported_builder.append(target_enum)
                else:
                    supported_but_missing_builder.append(target_enum)

        if self.LOGGING:
            print(f"these are supported but missing: {supported_but_missing_builder}")

        return supported_builder

    def initialise_interface(self):
        print("creating connection...", end="")
        self.go_get_raw("at")
        print(" done.\nresetting interface...", end="")
        self.go_get_raw("atz")
        print(" done.\ngetting vehicle information...", end="")
        self.get_vin()
        print(" done.\ngetting supported PIDs...", end="")
        self.SUPPORTED_PIDS = self.get_supported_pids()
        print(" done.\ngo!")

    def terminate(self):
        self.SERIAL_CONNECTION.close()

    def send_message(self, message: str) -> None:
        self.SERIAL_CONNECTION.write(f"{message}{self.NEWLINE_DELIMITER}".encode(self.ENCODING))
        self.SERIAL_CONNECTION.flush()

    def recieve_message(self, buffer_size: int) -> bytes:
        recieved_message = self.SERIAL_CONNECTION.read(buffer_size)
        return bytes.fromhex(recieved_message.hex())

    def parse_elm327_response(self, response: bytes) -> list[bytes]:
        response_str = response.decode(self.ENCODING)
        lines = response_str.split('\r')
        elm327_values = lines[1].strip().split(" ")
        return [s.encode('utf-8') for s in elm327_values]

    def go_get(self, service_mode: ServiceMode, command: CurrentDataCommand):  # -> bool | list[bytes]:
        return self.go_get_raw(f"{service_mode.value} {command.value.HEX}")

    def raw_send_recieve(self, message: str):
        if self.LOGGING:
            print(f"sending {message.encode(self.ENCODING)}...", end="")

        self.send_message(message)

        start_time = time.time()
        response = b''
        while (time.time() - start_time) < 2:
            response += self.recieve_message(self.SERIAL_CONNECTION.in_waiting)
            if b'>' in response:
                break

        if self.LOGGING:
            print(f"Received: {response.decode(self.ENCODING)}")

        return response

    def go_get_raw(self, message: str):
        response = self.raw_send_recieve(message=message)
        if self.LOGGING:
            print(f" recieved {(self.parse_elm327_response(response))}")
        parsed_response = self.parse_elm327_response(response)
        if parsed_response == [b'NO', b'DATA']:
            if self.LOGGING:
                print("NO DATA!")
            return False
        return parsed_response
