from enum import Enum
from elm327_interface.data_classes.command_cradle import CommandCradle
from elm327_interface.parameter_ids.encoded_pids.fuel_system_status import FuelSystemStatus


class CurrentDataCommand(Enum):
    """
    """

    ##################################################
    # PID GROUP 1/6 (0x01-0x20)
    ##################################################
    # 0x01
    # 0x02
    FUEL_SYSTEM_STATUS = CommandCradle(0x03, decoder=FuelSystemStatus.from_encoded_value)
    CALCULATED_ENGINE_LOAD = CommandCradle(0x04, _min=0, _max=100, _unit="%", formula="0 + (1/2.55) * A")
    ENGINE_COOLANT_TEMPERATURE = CommandCradle(0x05, _min=-40, _max=215, _unit="°C", formula="-40 + 1 * A")
    # 0x06
    # 0x07
    # 0x08
    # 0x09
    FUEL_PRESSURE__GAUGE_PRESSURE = CommandCradle(0x0B, _min=0, _max=765, _unit="kPa", formula="0 + 3 * 18")
    INTAKE_MANIFOLD_ABSOLUTE_PRESSURE = CommandCradle(0x0B, _min=0, _max=255, _unit="kPa")
    ENGINE_SPEED = CommandCradle(0x0C, _min=0, _max=16383.75, _unit="rpm", bit_length=16, formula="0 + 0.25 * A", round_to_decimals=0)
    VEHICLE_SPEED = CommandCradle(0x0D, _min=0, _max=255, _unit="%", round_to_decimals=2)
    # 0x0E
    INTAKE_AIR_TEMPERATURE = CommandCradle(0x0F, _min=-40, _max=215, _unit="°C", formula="-40 + 1 * A")
    MASS_AIR_FLOW_SENSOR_AIR_FLOW = CommandCradle(0x10, _min=0, _max=655, _unit="g/s", bit_length=16, formula="0 + 0.01 * A")
    THROTTLE_POSITION = CommandCradle(0x11, _min=0, _max=100, _unit="%", formula="0 + (1/2.55) * A", variation=(0.2, 0.2), round_to_decimals=0)
    # 0x12
    # 0x13

    # 0x14
    # 0x15
    # 0x16
    # 0x17
    # 0x18
    # 0x19
    # 0x1A
    # 0x1B

    # 0x1C
    # 0x1D
    # 0x1E
    RUN_TIME_SINCE_ENGINE_START = CommandCradle(0x1F, _min=0, _max=65535, _unit="s", bit_length=16)

    ##################################################
    # PID GROUP 2/6 (0x21-0x40)
    ##################################################
    DISTANCE_TRAVELLED_WITH_MIL_ON = CommandCradle(0x21, _min=0, _max=65535, _unit="km", bit_length=16)
    FUEL_RAIL_PRESSURE = CommandCradle(0x22, _min=0, _max=5177.265, _unit="kPa", formula="0 + 0.079 * A", bit_length=16)
    FUEL_RAIL_GAUGE_PRESSURE = CommandCradle(0x22, _min=0, _max=655350, _unit="kPa", formula="0 + 10 * A", bit_length=16)

    # 0x24
    # 0x25
    # 0x26
    # 0x27
    # 0x28
    # 0x29
    # 0x2A
    # 0x2B

    COMMANDED_EGR = CommandCradle(0x2C, _min=0, _max=100, _unit="%", formula="0 + (1/2.55) * A")
    EGR_ERROR = CommandCradle(0x2D, _min=-100, _max=99.2, _unit="%", formula="-100 + (1/1.28) * A")
    COMMANDED_EVAPORATIVE_PURGE = CommandCradle(0x2E, _min=0, _unit="%", _max=100, formula="0 + (1/2.55) * A")
    FUEL_TANK_LEVEL_INPUT = CommandCradle(0x2F, _min=0, _max=100, _unit="%", formula="0 + (1/2.55) * A")
    WARM_UPS_SINCE_CODES_CLEARED = CommandCradle(0x30, _min=0, _max=255, _unit=None)
    DISTANCE_TRAVELLED_SINCE_CODES_CLEARED = CommandCradle(0x31, _min=0, _max=65535, _unit="km", bit_length=16)
    EVAP_SYSTEM_VAPOR_PRESSURE = CommandCradle(0x32, _min=-8192, _max=8192, _unit="Pa", bit_length=16, formula=" 0 + 0.25 * A")
    ABSOLUTE_BAROMETRIC_PRESSURE = CommandCradle(0x33, _min=0, _max=255, _unit="kPa")

    # 0x34
    # 0x35
    # 0x36
    # 0x37
    # 0x38
    # 0x39
    # 0x3A
    # 0x3B

    # 0x3C
    # 0x3D
    # 0x3E
    # 0x3F

    ##################################################
    # PID GROUP 3/6 (0x41-0x60)
    ##################################################
    # 0x41
    CONTROL_MODULE_VOLTAGE = CommandCradle(0x42, _min=0, _max=66, _unit="v", formula="0 + 0.001 * A", bit_length=16)
    ABSOLUTE_LOAD_VALUE = CommandCradle(0x43, _min=0, _max=25700, _unit="%", formula="0 + (1/2.55) * A", bit_length=16)
    COMMANDED_AIR_FUEL_AQUIVELENT_RATIO = CommandCradle(0x44, _min=0, _max=2, _unit="ratio", formula="0 + (1/32768) * A", bit_length=16)
    RELATIVE_THROTTLE_POSITION = CommandCradle(0x45, _min=0, _max=100, _unit="%", formula="0 + (1/2.55) * A")
    AMBIENT_AIR_TEMPERATURE = CommandCradle(0x46, _min=-40, _max=215, _unit="°C", formula="-40 + 1 * A")

    # 0x47
    # 0x48
    # 0x49
    # 0x4A
    # 0x4B
    # 0x4C

    # 0x4D
    # 0x4E
    # 0x4F
    # 0x50
    # 0x51
    # 0x52
    # 0x53
    # 0x54
    # 0x55
    # 0x56
    # 0x57
    # 0x58
    # 0x59
    # 0x5A
    # 0x5B
    # 0x5C
    # 0x5D
    # 0x5E
    # 0x5F

    ##################################################
    # PID GROUP 4/6 (0x61-0x80)
    ##################################################
    # 0x61
    # 0x62
    # 0x63
    # 0x64
    # 0x65
    # 0x66
    # 0x67
    # 0x68
    # 0x69
    # 0x6A
    # 0x6B
    # 0x6C
    # 0x6D
    # 0x6E
    # 0x6F
    # 0x70
    # 0x71
    # 0x72
    # 0x73
    # 0x74
    # 0x75
    # 0x76
    # 0x77
    # 0x78
    # 0x79
    # 0x7A
    # 0x7B
    # 0x7C
    # 0x7D
    # 0x7E
    # 0x7F

    ##################################################
    # PID GROUP 5/6 (0x81-0xA0)
    ##################################################
    # 0x81
    # 0x82
    # 0x83
    # 0x84
    # 0x85
    # 0x86
    # 0x87
    # 0x88
    # 0x89
    # 0x8A
    # 0x8B
    # 0x8C
    # 0x8D
    # 0x8E
    # 0x8F
    # 0x90
    # 0x91
    # 0x92
    # 0x93
    # 0x94
    # 0x95
    # 0x96
    # 0x97
    # 0x98
    # 0x99
    # 0x9A
    # 0x9B
    # 0x9C
    # 0x9D
    # 0x9E
    # 0x9F

    ##################################################
    # PID GROUP 6/6 (0xA1-0xC0)
    ##################################################
    # 0xA1
    # 0xA2
    # 0xA3
    # 0xA4
    # 0xA5
    # 0xA6
    # 0xA7
    # 0xA8
    # 0xA9
    # 0xC0
    # 0xC3
    # 0xC4
