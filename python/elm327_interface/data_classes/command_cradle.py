from dataclasses import dataclass


@dataclass
class CommandCradle:
    HEX: str

    MIN_VALUE: float
    MAX_VALUE: float
    UNIT: str

    BIT_LENGTH: int

    FORMULA = None
    DECODER = None

    VARIATION: tuple[float, float]
    ROUND_TO_NUMBER_OF_DECIMALS: int = None

    def __init__(
            self,

            _hex: int,

            _min: float = None,
            _max: float = None,
            _unit: str = None,

            bit_length: int = 8,

            formula: str = "0 + 1 * A",
            decoder=None,

            variation: tuple[float, float] = (0.1, 0.1),
            round_to_decimals: int = 5
    ):
        self.HEX = format(_hex, "02x").upper()

        self.MIN_VALUE = _min
        self.MAX_VALUE = _max
        self.UNIT = _unit

        self.BIT_LENGTH = bit_length

        self.FORMULA = formula
        self.DECODER = decoder

        self.VARIATION = variation
        self.ROUND_TO_NUMBER_OF_DECIMALS = round_to_decimals
